#include <stdio.h>
/* This program will calculate the volume of a cone by obtaining radius and height from the user */
int main ()
{
	float pi = 3.14;
	float r;
	float h;
	float volume;
	
	printf("Enter the radius of the circle:");
	scanf("%f", &r);
	
	printf("Enter the height of the circle:");
	scanf("%f", &h);
	 
	volume = 3.14 * r * r * h/3;
	printf("The volume of the cone is equal to: %f\n", volume);
	
	return 0;
	
}
