#include <stdio.h>
/* This program will swap two numbers without using a temporary third variable */
int main ()

{
	int a;
	int b;
	
	printf("Enter any integer for a :");
	scanf("%d", &a);
	
	printf("Enter any integer for b :");
	scanf("%d", &b);
	      
printf("Before swapping a=%d b=%d",a,b);      
a=a+b;    
b=a-b;    
a=a-b;    
printf("\nAfter swapping a=%d b=%d",a,b);    
return 0;
}
